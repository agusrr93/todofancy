import React, { Component } from 'react';
import HomePage from './HomePage'
import {Text} from 'react-native'
import Register from './screens/Register/index'
import Login from './screens/Login/index'

export default class App extends Component {
  constructor(){
    super();
    this.state = {
        isLogin:false,
        isRegister:true,
        isHome:false,
        dataUser:[]
    };
    
    this.toLogin=this.toLogin.bind(this)
    this.toRegister=this.toRegister.bind(this)
    this.payloadLogin=this.payloadLogin.bind(this)
  }
  
  // { token:
  //   'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjOTJkYzkwY2NlNTNiMDAyY2ViYjEyMCIsImlhdCI6MTU1MzEzMTg2NiwiZXhwIjoxNTUzMjE4MjY2fQ.M6bOtSJlNck2rwoe6CaofN5GWOA1AYvXlfS3-ZnBSAU',
  //  id: '5c92dc90cce53b002cebb120',
  //  name: 'user',
  //  username: 'agusrr93',
  //  email: 'agusrr93@gmail.com' }
  toLogin(){
    this.setState({
      isLogin:true,
      isRegister:false,
      isHome:false
    },function(){
      
    })
  }
  
  payloadLogin(data){
    this.setState({
      isLogin:false,
      isRegister:false,
      isHome:true,
      dataUser:data
    },function(){
      console.log('ini data user',this.state.dataUser)
    })
  }

  toRegister(){
    this.setState({
      isLogin:false,
      isRegister:true,
      isHome:false
    },function(){
      
    })
  }
  
  logicRender(){
    if(this.state.isRegister===true){
       return <Register press={this.toLogin}></Register>
    }
    else if(this.state.isLogin===true){
      return <Login payload={this.payloadLogin} press={this.toRegister}></Login>
    }
    else if(this.state.isHome===true){
      return <HomePage user={this.state.dataUser}></HomePage>
    }
  }
  render() { 
    return(
      this.logicRender()
    )
  }
}
