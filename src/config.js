import Firebase from 'firebase';  

var config = {
    apiKey: "AIzaSyCIupQJAuZm_fLpXHSAGJn1dh2BHG-kdyA",
    authDomain: "hangman-ccadb.firebaseapp.com",
    databaseURL: "https://hangman-ccadb.firebaseio.com",
    projectId: "hangman-ccadb",
    storageBucket: "hangman-ccadb.appspot.com",
    messagingSenderId: "395201285126"
};

let app = Firebase.initializeApp(config);  

export const db = app.database();  