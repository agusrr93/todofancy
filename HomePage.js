import React, { Component } from 'react';
import Header from './components/header'
import {ImageBackground,View,Text} from 'react-native'
import List from './components/Category'
import {Item,Input,Icon, Button} from 'native-base'
import Todo from './components/Todo'
import {ScrollView,TextInput} from 'react-native'
import Action from './components/Action'
import Listing from './components/List'
import Loading from './components/loading'
import axios from 'axios'


import Img from './assets/nature.jpg'

export default class HomePage extends Component {
  constructor(props){
    super(props)
    
    this.state = {ListItemku:[],
      selected:0,ListTodoku:[],ListActive:'TODO',
      isModal:false,ListIdActive:'5c92edbdcce53b002cebb127',
      isInvite:false,isData:false,isEdit:false,
      token:'',activeTodo:0,renderTodo:0,todoText:'',
      isCreated:false,addText:''
      };

    this.handleCreate=this.handleCreate.bind(this)
    this.logicData=this.logicData.bind(this)
    this.pressTodo=this.pressTodo.bind(this)
    this.handleBack=this.handleBack.bind(this)
    this.handleModal=this.handleModal.bind(this)
    this.handleInvite=this.handleInvite.bind(this)
    this.handleEdit=this.handleEdit.bind(this)
    this.handleSubmit=this.handleSubmit.bind(this)
    this.DataFetcher=this.DataFetcher.bind(this)
    this.handleAddList=this.handleAddList.bind(this)
  }

  componentDidMount(){
    this.setState({
      token:this.props.user[0].token
    },
    function(){
        this.DataFetcher()
    })
  }
  
  DataFetcher(){
    var config = {
      headers: {'Authorization': "bearer " + this.state.token}
    };
    
     let self=this
     axios.post('https://todoappwebserver.azurewebsites.net/api/user/todoList/read','',config)
      .then(function (response) {
        var result=[]
        var todo=[]
        for(var i=0;i<response.data.todoList.length;i++){
            var obj={}
            var data={}
            obj.id=response.data.todoList[i]._id
            obj.todoList=response.data.todoList[i].name
            obj.todo=response.data.todoList[i].todo
            data.todo=response.data.todoList[i].todo
            result.push(obj)
            todo.push(data)
        }
        self.setState({
          ListItemku:result,
          ListTodoku:todo,
          isData:true
        },function(){
          console.log('ini list todoku',this.state.ListTodoku)
        })
      })
      .catch(function (error) {
          
      });
    
  }
  handleInvite(){
    this.setState({
      isModal:false,
      isInvite:true,
      isEdit:false
    })
  }

  handleCreate(){
    this.setState({
      isModal:false,
      isInvite:false,
      isEdit:false,
      isCreated:true
    })
  }

  handleAddList(){
    if(this.state.addText===''){
      alert('Please Fill Your Form First')
    }
    else{
      this.setState({
        isData:false
      })
      var config = {
        headers: {'Authorization': "bearer " + this.state.token}
      }
     let self=this
     axios.post('https://todoappwebserver.azurewebsites.net/api/user/todoList/create', {
        name:self.state.addText
      },config)
      .then(function (response) {
         self.DataFetcher()
      })
      .catch(function (error) {
        alert('error')
      });

    }
  }
  handleSubmit() {
      if(this.state.todoText===''){
        alert('Please Fill Your Form First')
      }
      else{
          this.setState({
            isData:false
          })
          var config = {
            headers: {'Authorization': "bearer " + this.state.token}
          }
         let self=this
         axios.post('https://todoappwebserver.azurewebsites.net/api/user/todo/create',{
            todoList: this.state.ListIdActive,
            name: this.state.todoText
        },config)
          .then(function (response) {
            console.log('dari login :',response.data.todoList.todo);

            self.DataFetcher()
          })
          .catch(function (error) {
            console.log(error);
          });

      }
  }

  justList(){
    return(
      <ImageBackground source={Img} style={{width: '100%', height: '100%'}}>
            <Header press1={this.handleInvite} press2={this.handleEdit} text={this.state.ListActive}></Header>
            <View style={{flex:1,flexDirection:'row'}}>
                <View style={{backgroundColor:'white',height:'100%',width:'45%'}}>
                <ScrollView>
                    <List icon='home' match={0} idMongoose='null' text='Inbox' selected={this.state.selected} press={this.pressTodo}/>
                    <List icon='calendar' match={1} idMongoose='null' selected={this.state.selected} text='Today' press={this.pressTodo}></List>
                    <List icon='calendar' match={2} idMongoose='null' selected={this.state.selected} text='Tomorrow' press={this.pressTodo}></List>
                    <List icon='calendar' match={3} idMongoose='null' selected={this.state.selected} text='Next Week' press={this.pressTodo}></List>
                    <List icon='star' match={4} idMongoose='null' selected={this.state.selected} text='Starred' press={this.pressTodo}></List>

                    {this.state.ListItemku.map((data,indeks)=>{
                        return(<List icon='list' idMongoose={data.id} match={indeks+5} press={this.pressTodo} selected={this.state.selected} text={data.todoList}/>)  
                    })}
                  
                    <Action press={this.handleCreate} icon='add' text='Create List'></Action>
                </ScrollView>
                
                </View>
                <View style={{height:'100%',width:'55%'}}>
                  <Item rounded style={{margin:10,backgroundColor:'#bfd8b6',width:'100%',height:30}}>
                      <Item>
                        <Icon onPress={this.handleSubmit} style={{marginLeft:10}} active name='add' />
                        <TextInput
                          style={{height: 40}}
                          onChangeText={(text) => this.setState({todoText:text})}
                          value={this.state.todoText}
                          placeholder='Add Todo'
                        />
        
                      </Item>
                  </Item>
                  
                  {this.state.ListTodoku[this.state.renderTodo].todo.map((data,indeks)=>{
                        return <Todo press={this.handleModal} name='star' maps={indeks} key={indeks} text={data.name}></Todo>  
                  })}

               </View>
            </View>
        </ImageBackground>
    )
  }

  listModal(){
    return(
      <ImageBackground source={Img} style={{width: '100%', height: '100%'}}>
            <Header press1={this.handleInvite} press2={this.handleEdit} text={this.state.ListActive}></Header>
              <View style={{flex:1,flexDirection:'row'}}>
                <View style={{backgroundColor:'white',height:'100%',width:'45%'}}>
                <ScrollView>
                    <List icon='home' idMongoose='null' match={0} text='Inbox' selected={this.state.selected} press={this.pressTodo}/>
                    <List icon='calendar' idMongoose='null' match={1} selected={this.state.selected} text='Today' press={this.pressTodo}></List>
                    <List icon='calendar' idMongoose='null' match={2} selected={this.state.selected} text='Tomorrow' press={this.pressTodo}></List>
                    <List icon='calendar' idMongoose='null' match={3} selected={this.state.selected} text='Next Week' press={this.pressTodo}></List>
                    <List icon='star' idMongoose='null' match={4} selected={this.state.selected} text='Starred' press={this.pressTodo}></List>

                    {this.state.ListItemku.map((data,indeks)=>{
                        return(<List idMongoose={data.id} icon='list' match={indeks+5} press={this.pressTodo} selected={this.state.selected} text={data.todoList}/>)  
                    })}
                  
                    <Action press={this.handleCreate} icon='add' text='Create List'></Action>
                </ScrollView>
                </View> 
                
                {this.state.isInvite===true&&this.ModalInvite()}   
                {this.state.isModal===true&&this.ModalTodo()} 
                {this.state.isEdit===true&&this.ModalEdit()}
                {this.state.isCreated===true&&this.ModalAdd()}   
                
                
            </View>
        </ImageBackground>
    )
  }

  ModalAdd(){
    return(
      <View style={{marginLeft:10,height:'100%',backgroundColor:'white',width:'50%'}}>
        <Button onPress={this.handleBack} style={{width:'100%',backgroundColor:'blue',justifyContent:'center'}}><Text style={{fontWeight:'bold'}}>Invite People</Text></Button>
        <Item rounded style={{marginRight:10,marginTop:15,backgroundColor:'#bfd8b6',width:'97%',height:30}}>
                    <Item>
                          <Icon onPress={this.handleAddList} name='add'></Icon>
                          <TextInput
                            style={{height: 40}}
                            onChangeText={(text) => this.setState({addText:text})}
                            value={this.state.addText}
                            placeholder='Add Todo'
                        />
                    </Item>  
            </Item>
            <Item rounded style={{marginRight:10,marginTop:15,backgroundColor:'#bfd8b6',width:'97%',height:30}}>
                    <Item>
                          <Icon style={{marginLeft:5}} active name='search' />
                          <Input placeholder='Search User'/>
                    </Item>  
            </Item>
            <Text style={{margin:10}}>Already in Group : </Text>
                <Listing icon='person' match='user' text='agusrr93' press={()=>{alert('hhe')}}></Listing>
                <Listing icon='person' match='user' text='ditohafidz' press={()=>{alert('hhe')}}></Listing>
            <ScrollView>
              <Text style={{margin:10}}>Ready To Invite : </Text>
                <Listing icon='person' match='user' text='agusrr93' press={()=>{alert('hhe')}}></Listing>
                <Listing icon='person' match='user' text='ditohafidz' press={()=>{alert('hhe')}}></Listing>
            </ScrollView>
      </View>
    )
  }

  ModalEdit(){
    return(
      <View style={{marginLeft:10,height:'100%',backgroundColor:'white',width:'50%'}}>
        <Button onPress={this.handleBack} style={{width:'100%',backgroundColor:'blue',justifyContent:'center'}}><Text style={{fontWeight:'bold'}}>Invite People</Text></Button>
        <Item rounded style={{marginRight:10,marginTop:15,backgroundColor:'#bfd8b6',width:'97%',height:30}}>
                    <Item>
                          <Input placeholder='Edit List'/>
                    </Item>  
            </Item>
            <Item rounded style={{marginRight:10,marginTop:15,backgroundColor:'#bfd8b6',width:'97%',height:30}}>
                    <Item>
                          <Icon style={{marginLeft:5}} active name='search' />
                          <Input placeholder='Search User'/>
                    </Item>  
            </Item>
            <Text style={{margin:10}}>Already in Group : </Text>
                <Listing icon='person' match='user' text='agusrr93' press={()=>{alert('hhe')}}></Listing>
                <Listing icon='person' match='user' text='ditohafidz' press={()=>{alert('hhe')}}></Listing>
            <ScrollView>
              <Text style={{margin:10}}>Ready To Invite : </Text>
                <Listing icon='person' match='user' text='agusrr93' press={()=>{alert('hhe')}}></Listing>
                <Listing icon='person' match='user' text='ditohafidz' press={()=>{alert('hhe')}}></Listing>
            </ScrollView>
      </View>
    )
  }

  ModalInvite(){
    return(
      <View style={{marginLeft:10,height:'100%',backgroundColor:'white',width:'50%'}}>
        <Button onPress={this.handleBack} style={{width:'100%',backgroundColor:'blue',justifyContent:'center'}}><Text style={{fontWeight:'bold'}}>Invite People</Text></Button>
            <Item rounded style={{marginRight:10,marginTop:15,backgroundColor:'#bfd8b6',width:'97%',height:30}}>
                    <Item>
                          <Icon style={{marginLeft:5}} active name='search' />
                          <Input placeholder='Search User'/>
                    </Item>  
            </Item>
            <Text style={{margin:10}}>Already in Group : </Text>
                <Listing icon='person' match='user' text='agusrr93' press={()=>{alert('hhe')}}></Listing>
                <Listing icon='person' match='user' text='ditohafidz' press={()=>{alert('hhe')}}></Listing>
            <ScrollView>
              <Text style={{margin:10}}>Ready To Invite : </Text>
                <Listing icon='person' match='user' text='agusrr93' press={()=>{alert('hhe')}}></Listing>
                <Listing icon='person' match='user' text='ditohafidz' press={()=>{alert('hhe')}}></Listing>
            </ScrollView>
      </View>
    )
  }
  
  ModalTodo(){
    return(
      <View style={{marginLeft:10,height:'100%',backgroundColor:'white',width:'50%'}}>
                  <Button onPress={this.handleBack} style={{width:'100%',backgroundColor:'blue',justifyContent:'center'}}><Text style={{fontWeight:'bold'}}>Back</Text></Button>
                  <List icon='home' text='Todo' match={'nama'}></List>
                  <View style={{height: '70%'}} >
                  <ScrollView>
                      <Item rounded style={{margin:10,backgroundColor:'#bfd8b6',width:'90%',height:30}}>
                          <Item>
                            <Icon style={{marginLeft:10}} active name='add' />
                            <Input placeholder='Add Subtask'/>
                          </Item>  
                      </Item>
                      <Listing icon='list' text='Ngaji' match={'duedate'}></Listing>
                      <Listing icon='Listing' text='Bersih bersih' match={'duetime'}></Listing>
                      <Item rounded style={{margin:10,backgroundColor:'#bfd8b6',width:'90%',height:40}}>
                          <Item>
                            <Icon style={{marginLeft:10}} active name='document' />
                            <Input placeholder='Notes'/>
                          </Item>  
                      </Item>
                      
                      <Listing icon='document' text='Due Today' match={'duedate'}></Listing>

                      <Text style={{fontSize:20}}>Deadline :</Text>
                      <Listing icon='calendar' text='Due Today' match={'duedate'}></Listing>
                      <Listing icon='calendar' text='Reminder at 9:00' match={'duetime'}></Listing>
                  </ScrollView>
                  </View>
                  
                                     
                  <Item rounded style={{position: "absolute", bottom: 0,margin:10,backgroundColor:'#bfd8b6',width:'100%',height:40}}>
                          <Item>
                            <Icon style={{marginLeft:10}} active name='chatbubbles' />
                            <Input placeholder='comment'/>
                         </Item>  
                  </Item>
               </View>
        )
  }
  
  pressTodo(data){
    this.setState({
        selected:data.select,
        ListActive:data.text,
        isModal:false,
        isInvite:false,
        isEdit:false,
        isCreated:false
    },function(){
        if(this.state.selected<5){
          this.setState({
            renderTodo:0
          })
        }
        else if(this.state.selected>=5){
          this.setState({
            renderTodo:this.state.selected-5
         })
        }

        if(data.idActive!=='null'){
          this.setState({
             ListIdActive:data.idActive
          })
        }
        else if(data.idActive==='null'){
          this.setState({
            ListIdActive:'5c92edbdcce53b002cebb127'
          })
        }
    })
  }
  

  handleEdit(){
    this.setState({
      isModal:false,
      isInvite:false,
      isEdit:true,
      isCreated:false
    })
  }

  handleModal(){
    this.setState({
      isModal:true,
      isInvite:false,
      isEdit:false,
      isCreated:false
    })
  }

  handleBack(){
    this.setState({
      isModal:false,
      isInvite:false,
      isEdit:false,
      isCreated:false
    })
  }
  logicData(){
    if(this.state.isData===false){
      return <Loading></Loading>
    }
    else if(this.state.isData===true){
      return (this.state.isCreated===false&&this.state.isModal===false&&this.state.isInvite===false&&this.state.isEdit===false)?this.justList():this.listModal()  
    }
  }
  render() { 
    return(
      this.logicData()   
    )
  }
}
