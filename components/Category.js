
import React, { Component } from 'react';
import {View,StyleSheet} from 'react-native';
import {ListItem,Left,Right,Text,Icon,Button,Body} from 'native-base'



export default class Category extends Component {
  activeList(){
    return(
      <View>
          <ListItem icon style={styles.listActive} onPress={() => this.props.press({idActive:this.props.idMongoose,select:this.props.match,text:this.props.text})}>
          <Left>
            <Button style={{ backgroundColor: "#FF9501" }}>
              <Icon active name={this.props.icon} style={{fontSize:10}}/>
            </Button>
          </Left>
          <Body>
            <Text style={styles.text}>{this.props.text}</Text>
          </Body>
          <Right>
            <Text style={{fontSize:12}}>1</Text>
          </Right>
        </ListItem>
      </View>
    )
  }

  passiveList(){
    return(
      <View>
          <ListItem icon style={styles.list} onPress={() => this.props.press({idActive:this.props.idMongoose,select:this.props.match,text:this.props.text})}>
          <Left>
            <Button style={{ backgroundColor: "#FF9501" }}>
              <Icon active name={this.props.icon} style={{fontSize:10}}/>
            </Button>
          </Left>
          <Body>
            <Text style={styles.text}>{this.props.text}</Text>
          </Body>
          <Right>
            <Text style={{fontSize:12}}>1</Text>
          </Right>
        </ListItem>
      </View>
    )
  }
  render() {  
      return(
        (this.props.match===this.props.selected)?this.activeList():this.passiveList()
      )
  }
}

const styles=StyleSheet.create({
  listActive:{
    margin:10,backgroundColor:'blue'
  },
  list:{
    margin:10
  },
  text:{
    fontFamily: 'sans-serif-medium',fontSize:12,padding:10,fontWeight:'normal'
  }
})