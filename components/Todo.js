
import React, { Component } from 'react';
import {View,StyleSheet} from 'react-native';
import {ListItem,Left,Right,Text,Icon,Button,Body} from 'native-base'

export default class Todo extends Component {
  render() {
    console.log(this.props);
    
    return (
      <View>
              <ListItem icon style={styles.list} onPress={this.props.press}>
                    <Left style={{margin:10}}>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon style={{fontSize:10}} active name='pint' />
                      </Button>
                    </Left>
                    <Body>
                      <Text style={styles.text}>{this.props.text}</Text>
                    </Body>
                    <Right>
                      <Icon active name='star' style={{fontSize:20}}/>
                    </Right>
              </ListItem>
      </View>  
      
    );
  }
}

const styles=StyleSheet.create({
  list:{
    backgroundColor:'white',width:'90%',margin:5
  },
  text:{
    fontFamily: 'sans-serif-medium',fontSize:12,padding:10,fontWeight:'normal'
  }
})