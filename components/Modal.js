import React, { Component } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import {Modal} from "react-native";

export default class ModalTester extends Component {

  render() {
    return (
        <View>
            <Modal>
            <View style={{ flex: 1 }}>
                <Text>I am the modal content!</Text>
            </View>
            </Modal>
      </View>
    );
  }
}