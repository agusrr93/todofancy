import React, { Component } from 'react';
import { Text, View,StyleSheet} from 'react-native';
import {Icon} from 'native-base'

export default class Header extends Component {
  render() {
    return (
      <View>
            <View style={styles.container}>
                <Text style={styles.text}>{this.props.text}</Text>
                
                    <Icon onPress={this.props.press1} style={{position:'absolute',right:85,color:'orange'}} name='person'></Icon>
                    <Icon onPress={this.props.press2} style={{position:'absolute',right:55,color:'orange'}} name='document'></Icon>
                    <Icon onPress={this.props.press3} style={{position:'absolute',right:25,color:'orange'}} name='search'></Icon>
                
            </View>
      </View>  
      
    );
  }
}

const styles=StyleSheet.create({
  container:{
    backgroundColor:"#a4ddab",height:70,flexDirection:'row',alignItems:'center'
  },
  text:{
    fontFamily: 'sans-serif-medium',fontSize:20,padding:10,fontWeight:'bold'
  }
})