
import React, { Component } from 'react';
import {View,StyleSheet} from 'react-native';
import {ListItem,Left,Right,Text,Icon,Button,Body} from 'native-base'



export default class Listing extends Component {
  passiveList(){
    return(
      <View>
          <ListItem icon style={styles.list} onPress={() => this.props.press({select:this.props.match,text:this.props.text})}>
          <Left>
            <Button style={{ backgroundColor: "#FF9501" }}>
              <Icon active name={this.props.icon} style={{fontSize:10}}/>
            </Button>
          </Left>
          <Body>
            <Text style={styles.text}>{this.props.text}</Text>
          </Body>
        </ListItem>
      </View>
    )
  }
  render() {  
      return(
        this.passiveList()
      )
  }
}

const styles=StyleSheet.create({
  list:{
    margin:10
  },
  text:{
    fontFamily: 'sans-serif-medium',fontSize:12,padding:10,fontWeight:'normal'
  }
})