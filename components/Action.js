
import React, { Component } from 'react';
import {View,StyleSheet} from 'react-native';
import {ListItem,Left,Right,Text,Icon,Button,Body} from 'native-base'

export default class Category extends Component {
  render() {
    return (
      <View>
              <ListItem icon style={styles.list} onPress={this.props.onPress}>
                    <Left>
                      <Button style={{ backgroundColor: "#172056" }}>
                        <Icon onPress={this.props.press} active name={this.props.icon} style={{fontSize:10}}/>
                      </Button>
                    </Left>
                    <Body>
                      <Text style={styles.text}>{this.props.text}</Text>
                    </Body>
              </ListItem>
      </View>  
      
    );
  }
}

const styles=StyleSheet.create({
  list:{
    margin:10
  },
  text:{
    fontFamily: 'sans-serif-medium',fontSize:12,padding:10,fontWeight:'normal'
  }
})