import React, { Component } from 'react';
import { AppRegistry, ScrollView, Text, StyleSheet} from 'react-native';
import {Icon,Button} from 'native-base'

styles=StyleSheet.create({
    scroller: {
        flex: 1,
    }
});

export default class AndroidFonts extends Component{
  render (){
    return(
      <ScrollView style={styles.scroller}>
         <Button primary>
                        <Icon name="business" />
         </Button>
        
        <Text style={{fontFamily: 'normal',fontSize:30}}>  normal </Text>
        <Text style={{fontFamily: 'notoserif',fontSize:30}}>  notoserif </Text>
        <Text style={{fontFamily: 'sans-serif',fontSize:30}}>  sans-serif </Text>
        <Text style={{fontFamily: 'sans-serif-light',fontSize:30}}>  sans-serif-light </Text>
        <Text style={{fontFamily: 'sans-serif-thin',fontSize:30}}>  sans-serif-thin </Text>
        <Text style={{fontFamily: 'sans-serif-condensed',fontSize:30}}>  sans-serif-condensed </Text>
        <Text style={{fontFamily: 'sans-serif-medium',fontSize:30}}>  sans-serif-medium </Text>
        <Text style={{fontFamily: 'serif',fontSize:30}}>  serif </Text>
        <Text style={{fontFamily: 'Roboto',fontSize:30}}>  Roboto </Text>
        <Text style={{fontFamily: 'monospace',fontSize:30}}>  monospace </Text>        
      </ScrollView>
    );
  }
}
